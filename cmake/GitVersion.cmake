include_guard()

find_package(Git REQUIRED)

execute_process(
  COMMAND "${GIT_EXECUTABLE}" "describe" "--always" "--tags" "HEAD"
  WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
  OUTPUT_VARIABLE _OUTPUT
  ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)

if(${_OUTPUT} MATCHES "^([0-9]+)\\.([0-9]+)\\.([0-9]+)-([0-9]+).*$")
  set(GIT_VERSION_MAJOR ${CMAKE_MATCH_1})
  set(GIT_VERSION_MINOR ${CMAKE_MATCH_2})
  set(GIT_VERSION_PATCH ${CMAKE_MATCH_3})
  set(GIT_VERSION ${GIT_VERSION_MAJOR}.${GIT_VERSION_MINOR}.${GIT_VERSION_PATCH})
  set(GIT_RELEASE ${CMAKE_MATCH_4})
else()
  message(FATAL_ERROR "GitVersion: Invalid version string - ${_GIT_OUTPUT}")
endif()
