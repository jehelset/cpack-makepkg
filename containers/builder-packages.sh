#!/bin/sh
set -e 

declare -a packages=(
    cmake git fakeroot curl namcap)

sudo pacman -S --noconfirm --needed ${packages[@]}
sudo pacman -Scc --noconfirm
