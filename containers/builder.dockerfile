FROM archlinux/archlinux:base-devel

ENV LANG=en_US.UTF-8

RUN pacman -Syu --noconfirm --debug ;

RUN echo "%wheel ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers ;
RUN useradd -m -g wheel -p ci ci

COPY builder-packages.sh /tmp
RUN su ci -c /tmp/builder-packages.sh

COPY builder-aur-packages.sh /tmp
RUN su ci -c /tmp/builder-aur-packages.sh

USER "ci"
CMD ["/bin/bash"]
