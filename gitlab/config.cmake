cmake_minimum_required(VERSION 3.19 FATAL_ERROR)

set(SCRIPT_DIR   ${CMAKE_CURRENT_LIST_DIR})
set(REPORTS_DIR  ${SCRIPT_DIR}/reports)
set(PACKAGES_DIR ${SCRIPT_DIR}/packages)

get_filename_component(PROJECT_DIR ${SCRIPT_DIR} DIRECTORY)

set(BUILD_DIR ${PROJECT_DIR}/build)

set(CI_API_URL $ENV{CI_API_V4_URL})
set(CI_PROJECT_ID $ENV{CI_PROJECT_ID})
set(CI_JOB_TOKEN $ENV{CI_JOB_TOKEN})

include(${PROJECT_DIR}/cmake/GitVersion.cmake)

macro(ci_execute_process)
  execute_process(${ARGN} WORKING_DIRECTORY ${PROJECT_DIR} COMMAND_ERROR_IS_FATAL ANY)
endmacro()
