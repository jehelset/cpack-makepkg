include(${CMAKE_CURRENT_LIST_DIR}/config.cmake)

ci_execute_process(
  COMMAND ${CMAKE_COMMAND} tests
    -B tests/build)
ci_execute_process(
  COMMAND ${CMAKE_COMMAND} --build tests/build)
