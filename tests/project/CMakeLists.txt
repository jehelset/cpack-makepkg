cmake_minimum_required(VERSION 3.19)
project(cpack-makepkg-project)

install(
  FILES cpack-makepkg.py
  DESTINATION .)

install(
  FILES cpack-makepkg.md
  DESTINATION .)

install(
  FILES cpack-makepkg.dat
  DESTINATION assets)

#NOTE: configure cpack-makepkg - jeh
get_filename_component(CPACK_PROPERTIES_FILE
  "../../CPackMakepkgProperties.cmake"
  ABSOLUTE)
include(CPack)

#NOTE: used to check results - jeh
set(LAYOUT "PROJECT")
configure_file(
  "../CheckPackages.cmake.in"
  ${PROJECT_BINARY_DIR}/CheckPackages.cmake
  @ONLY)
